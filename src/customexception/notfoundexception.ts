import { HttpException, HttpStatus } from "@nestjs/common";

export class NOTFOUND extends HttpException {
    constructor() {
        super('NOT FOUND', HttpStatus.NOT_FOUND)
    }
}