import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, ValidationPipe,  createParamDecorator, CacheKey, CacheTTL, UseInterceptors, CacheInterceptor, UsePipes, ForbiddenException, Res, Req } from '@nestjs/common';
import { LeaveService } from './leave.service';
import { ApiTags } from '@nestjs/swagger';

import {  Request, Response } from 'express';
import { JwtService } from '@nestjs/jwt';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/user.entity';
import { UserLeave } from './entity/leave.entity';


@ApiTags('leave')
@UsePipes(ValidationPipe)
@Controller('leave')
export class LeaveController {
  
  constructor(private readonly leaveService: LeaveService, private jwtService:JwtService) 
  {}

  @Post()
  applyLeave(@Body() newdata: User): Promise<string>{
    return this.leaveService.applyLeave(newdata);
  }

  @Get()
  @CacheKey('allleaves')
  @CacheTTL(15)
  getLeaveData(): Promise<User[]> {
    return this.leaveService.getLeaveData();
  }
  @Get('leavedata/:leaveDate')
  getByDate(@Param('leaveDate') leaveDate: string): Promise<User[]> {
    return this.leaveService.getLeaveByDate(leaveDate)
    .then((result) => {
      if (result) {     
        return result;      
      } else {      
        throw new ForbiddenException();  
      }     
    })     
    .catch (() => {     
      throw new ForbiddenException();    
    });
  }

  @Post('/login')
    async login(@Body() login: UserLogin,@Res({passthrough:true})response:Response) {
      return this.leaveService.findOne(login,response);
    }
    
   @Get('/userlogin')
    async users(@Req() request:Request){
      return this.leaveService.findUser(request);
    }

    @Post('/logout')
    async logout(@Res({passthrough:true}) response:Response){
       return this.leaveService.logout(response);
    }

  @Put('user/:id/:date')
  update(@Param('id') id: number, @Body() date: User) {
    return this.leaveService.updateUser(id, date);
  }

  @Put('leave/:id/:date') updateLeave(
    @Param('id') id: number,
    @Body() date: UserLeave,
  ) {
    return this.leaveService.updateLeave(id, date);
  }

  
  @Delete('/:id')
  delete(@Param('id') id: number){
        return this.leaveService.deleteLeave(id);
  }
  
}