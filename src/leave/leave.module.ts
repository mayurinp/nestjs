import { AuditMiddleware } from './../middlewares/audit.middleware';
import {CacheModule, MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import {LeaveController}from './leave.controller';
import { LeaveService } from './leave.service';
import { JwtModule } from '@nestjs/jwt';
import { UserLogin } from './entity/login.entity';
import { UserLeave } from './entity/leave.entity';
import { User } from './entity/user.entity';

@Module({
    imports: [TypeOrmModule.forFeature([User, UserLeave,UserLogin]),JwtModule.register({
        secret:'secret',
        signOptions:{expiresIn:'1hr'}}),
    
        CacheModule.register({
            ttl: 5,
            max: 100,
        })],
    controllers:[LeaveController],
    providers:[LeaveService]
})
export class LeaveModule implements NestModule{
    configure(consumer: MiddlewareConsumer) {
       consumer
        .apply(AuditMiddleware)
        .forRoutes({path: 'leave/*', method: RequestMethod.DELETE})
    }
}