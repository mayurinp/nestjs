
import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class UserLeave {

  @ApiProperty({
    type: Number,
    description: 'The id ',
    default: '',
  })  
  @PrimaryGeneratedColumn()
  @IsInt()
  id: Number;

  @ApiProperty({
    type: Number,
    description: 'The availableLeaves ',
    default: '',
  })
  @Column()
  @IsInt()
  availableLeaves: number;

  @ApiProperty({
    type: Number,
    description: 'The leavesTaken ',
    default: '',
  })
  @Column()
  @IsInt()
  leavesTaken: number;

  @ApiProperty({
    type: Number,
    description: 'The totalLeaves',
    default: '',
  })
  @Column()
  @IsInt()
  totalLeaves: number;

  @ManyToOne(() => User, (user) => user.userleave)
  user: User;
  
  
}