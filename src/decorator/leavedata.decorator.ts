
import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { Body } from '@nestjs/common';
export const leaveData = createParamDecorator(
    (data: string, ctx: ExecutionContext) => {
      const request = ctx.switchToHttp().getRequest();
      const leave = request.body;
  
      return data ? leave && leave[data] : leave;
    },
  );
